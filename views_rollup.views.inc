<?php

/**
 * Implementation of hook_views_plugins
 */
function views_rollup_views_plugins() {
  global $db_type;

  // This module only works on MySQL-based backends.
  if ($db_type == 'mysql' || $db_type == 'mysqli') {
    $plugins = array(
      'query' => array(
        'views_query_rollup' => array(
          'title' => t('SQL Query (with ROLLUP support)'),
          'help' => t('Query will be generated and run using the Drupal database API with additional support for the ROLLUP modifier.'),
          'handler' => 'views_plugin_query_rollup',
          'parent' => 'views_query',
        ),
      ),
    );
  }

  return $plugins;
}

/**
 * Implementation of hook_views_data_alter().
 */
function views_rollup_views_data_alter(&$data) {
  foreach ($data as $table => &$table_data) {
    if (isset($table_data['table']['base'])) {
      // If query class is set and it contains views_query, we can swap it out.
      $is_views_query = isset($table_data['table']['base']['query class']) && ($table_data['table']['base']['query class'] == 'views_query');
      // If query class isn't set, we can assume that it's using views_query.
      if ($is_views_query || empty($table_data['table']['base']['query class'])) {
        $table_data['table']['base']['query class'] = 'views_query_rollup';
      }
    }
  }
}
