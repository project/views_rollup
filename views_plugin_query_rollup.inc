<?php

class views_plugin_query_rollup extends views_plugin_query_default {

  function option_definition() {
    $options = parent::option_definition();

    $options['use_rollup'] = array(
      'category' => 'advanced',
      'title' => t('Use WITH ROLLUP'),
      'value' => $this->options['use_rollup'] ? t('Yes') : t('No'),
      'desc' => t('Add total row to bottom of result set.'),
    );

    return $options;
  }
  
  /**
   * Add settings for the ui.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['use_rollup'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use WITH ROLLUP'),
      '#description' => t('Adds the ROLLUP modifier when grouping is enabled to include a total row at the end of the result set.<br/><strong>NOTE: This will have no effect until grouping is also enabled. It also cannot be used along with a sort criteria.</strong>'),
      '#default_value' => !empty($this->options['use_rollup']),
    );
  }
  
  /**
   * Set the query to use WITH ROLLUP.
   */
  function set_use_rollup($value = TRUE) {
    if ($value) {
      $this->use_rollup = $value;
    }
  }

  function build(&$view) {
    if (!empty($this->options['use_rollup'])) {
      $this->set_use_rollup();
    }

    parent::build($view);
  }

  /**
   * Generate a query and a countquery from all of the information supplied
   * to the object.
   *
   * @param $get_count
   *   Provide a countquery if this is true, otherwise provide a normal query.
   */
  function query($get_count = FALSE) {
    $query = parent::query($get_count);
    
    if ($this->use_rollup) {
      // WITH ROLLUP cannot be used with statements using ORDER BY.
      if (!$this->orderby) {
        if (!count($this->having)) {
          $query .= ' WITH ROLLUP';
        }
        else if (count($this->having)) {
          $query = preg_replace("/(\\s+HAVING)/ui", " WITH ROLLUP$1", $query);
        }
      }
      else if ($this->view->live_preview && !$get_count) {
        drupal_set_message(t('You cannot use ROLLUP because you are also using a sort criteria.'), 'warning');
      }
    }
  
    return $query;
  }

}
